Open Source Hardware – Basics, Prototyping and Business Model Generation
===

## General

This module teaches the basics of open source hardware – specifically documentation, business model generation and legal issues.

Find a summarising module description [here](module-description.md) and a more detailed description below.

Slides rely on [reveal.js](https://revealjs.com/) and are written in markdown.

To launch them in a presentable mode just copy the markdown source into a new document of any instance of HedgeDoc (e.g. [this one](https://demo.hedgedoc.org/)).

## 12.11. Introduction

- What is this course about?
- presentation of all lecturers
- entrepreneurship basics
- Why, What, How OSH + examples
- potentials for entrepreneurship

**Plan:**

- Sigrid: Intro [15min]
- Sigrid: general introduction to Business Models [60min]
- Martin: Introduction to Open Source Hardware [60min]
    - [slide show](https://pad2.opensourceecology.de/p/Sk4sMZeYD#/) ([source](slides/s-intro-osh.md))
- Sigrid: Group forming & discussion [90min]
    - [Group] What should be open and why?
        - [group action: opening](group-actions/G-intro-opening.md)

## 19.11. Documentation of OSH

- what is the key difference between open and closed documentation?
- why is open source documentation not a single checklist?
- what are the main elements (to start with) of Open Source Projects?
- Example Solar Charger
- what happens when you build your own hardware and look at it from the documentations side?
- from documentation to collaboration
- platforms, tools, improvements

**Plan:**

- Sigrid: Intro [15min]
- Timm: Why documentation is key and a never ending story [45min]
    - [slide show](https://pad2.opensourceecology.de/p/BJwuhtaYP#) ([source](slides/s-docu-osh.md))
- Timm: Exercise part 1 - How to build a simple Solar Charger [90min]
    - → introduction (tools, materials, online-collaboration) + FAQ [15min]
    - → form groups + preparation [15min]
    - → prototyping + documentation [60min]
- Timm: Exercise part 2 - Decentralised collaboration [60min]
    - → new setup - new challenge
    - → how to improve collaboration?
    - → what are the learnings and what is next?
- Sigrid: Feedback and Checkout [15min]


## 26.11. Deep Dive into OSH; Entrepreneurship for OSH

- IP Law basics & role of OSH
    - crush the patent illusion
    - IP law basics (copyright & patent law)
    - tl;dr OSH licenses & compatibility
- How to make it open?
    - Documentation blocks (editable source files etc.)
- Open revenue streams
- current efforts: standardisation & OSH
- guest: DIN

**Plan:**

- Martin: Deep Dive into OSH [60min]
    - [slide show](https://pad2.opensourceecology.de/p/H13RyulYw#/) ([source](slides/s-deep-osh.md))
- Sigrid: Open Entrepreneurship [60min]
- Martin & Sigrid: Open value proposition [60min]
    - [group action: value proposition](group-actions/G-deep-value.md)
- Martin: DIN SPEC 3105 [15min]
    - [PDF](slides/s-deep-dinspec.pdf) ([source](slides/s-deep-dinspec.odp))
- Amelie: DIN [30min]
    - [PDF](https://cloud.opensourceecology.de/s/sniXYnWoo9j6pHP)

## 03.12. Ideation I

Material is published under a free/open license [here](https://opencircularity.info/osb/).

## Contributing

### Point of contact

Giving feedback or contributing to the material directly e.g. via merge request is always highly appreciated. This is what keeps the thing rolling.

In case you want to get in touch with the core team of maintainers, for the moment the easiest would be to simply drop an issue in which you explain your motivation for the contact. Happy to e-meet you then :)

### File name convention

It's fairly easy:

[type]-[module]-[sub-module]

S – slides
G – group action

EXAMPLE 1:

`S-intro-osh.md` = slides for [Introduction module](#introduction), specifically for the OSH part (as there're also slides for the entrepreneurship part in the same module)

EXAMPLE 2:

`S-docu.md` = slides for [Documentation module](#documentation), apparently there're no submodules

EXAMPLE 3:

`G-deep-value.md` = description for group action within the [Deep Dive module](#deep-dive), specifically the value creation exercise