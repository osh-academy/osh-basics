[Lecture] Drawing & Copyright
===

## Meta

- 5min
- 1 student
- 1 small piece of paper

## Plan

- student comes to the lecturer
- student draws a quick sketch on a piece of paper
- conversation:
    - Lecturer: Nice, so now it's copyright-protected. Can I have it?
    - Studen: sure
    - Lecturer: He just said 'sure'. What else could have been the response now?
    [Discussion]
    - Essentially I need a permission. Preferably in written form. A contract.
