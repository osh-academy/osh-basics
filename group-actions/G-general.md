# General remarks on group actions

## optional elements

### 4 thinking hats

inspired by [Six Thinking Hats](https://en.wikipedia.org/wiki/Six_Thinking_Hats)

- assign roles in group:
    - moderator & documenter, guiding & documenting the discussion 
    - dreamer, providing unfiltered ideas
    - realist, translating the dreamers ideas into things that could actually work (neutral perspective!)
    - critic, finding gaps and defects of ideas after processed by the realist
- one iteration loop goes from dreamer to realist to critic, 1min each, moderator guides this (while keeping track of the time)
- the next iteration loop starts again with the dreamer who now can provide ideas to fill & fix the gaps & defects detected by the critic
