[Group] What should be open and why?
===

## Meta

- 30min
- People form groups of 3…5

## Plan

### 1st round | think for yourself [5min]

- What should be open source?
- And why would that make this product special?\
  → How is it better than e.g. a super cheap, but proprietary copy from China?

### 2nd round | group challenge [20min]

- each group member pitches 1 own idea to the rest of the group
- group challenges the idea [5min each] with questions like:
    - Use case: What problem does it solve?
    - What is it exactly? What’s new, what can be reused from other designs? (this helps you finding your position in the ecosystem)
    - How is this better than existent solutions?
    - Target group: For whom are we building this?
    - … and with whom? (e.g. collaboration with manufacturers as Sparkfun Electronics)

**NOTE:** This obviously too less time for an exhaustive discussion; cutting the discussion off when it's most interesting will keep brains active & interested.

### 3rd round | pitch [10min]

- Each group decides on 1 idea to pitch + 1 person to pitch it.
- This person pitches the idea to the whole course:
    - begin with a catchy phrase as an opener
    - 1 min per pitch

**NOTE:** Questions students should have in mind after this:
- Can we collaborate within this course? This is already a community :)
- What is our position in the ecosystem? How can we contribute, how can we benefit from others?
