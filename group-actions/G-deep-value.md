[Group] Open value proposition
===

## Meta

- 60min
- class is already divided in 10 groups of 3…5 

## Plan

### Preparation [10min]

- For each group 1 of the following 5 products is randomly assigned, so for 10 groups we get 2 groups per product (e.g. everyone writes a number between 1…5 in the chat, moderator oversees whether all numbers are more or less equally represented).
    - bicycle
    - e-scooter
    - COVID-19 ventilator
    - laptop
    - smartphone

### Create a value proposition canvas [35min]

- **Situation:** Each group just won a public grant to make this product 
    - a) open source and 
    - b) suitable for sustainable business (either as an own company or others).
- Each group creates a value proposition canvas to be presented to the funder (not actually the case, just for the exercise here).

as here <https://cloud.opensourceecology.de/s/K3czkraq9Cn9ixs>

or from Sigrid :)

### Compare [15min]

- Groups that worked on the same product sit together and compare & discuss their results
    - everyone meets in breakout room 1 of each product
