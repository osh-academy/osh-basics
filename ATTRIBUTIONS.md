Attributions
===

| IMAGE                                               | NAME & SOURCE                                                                                                        | AUTHOR       | LICENSE                                                               |
|-----------------------------------------------------|----------------------------------------------------------------------------------------------------------------------|--------------|-----------------------------------------------------------------------|
| ![](graphics/noun_Wind_3377555.svg)                 | [wind](https://thenounproject.com/search/?q=3377555&i=3377555)                                                       | LAFS         | [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/us/legalcode) |
| ![](graphics/Safecast_bGeigie_Nano_opened.jpg)      | [Safecast bGeigie Nano opened](https://upload.wikimedia.org/wikipedia/commons/1/18/Safecast_bGeigie_Nano_opened.jpg) | Jhelebrant   | [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)        |
| ![](graphics/Wikipedia-logo-v2.svg)                 | [Wikipedia Logo v2](https://commons.wikimedia.org/wiki/File:Wikipedia-logo.svg)                                      | Nohat        | [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0)        |
| ![](graphics/noun_Missing Puzzle Piece_1445716.svg) | [Missing Puzzle Piece](https://thenounproject.com/search/?q=1445716&i=1445716)                                       | Andrew Doane | [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0)        |
