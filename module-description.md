# Module Description

|Data Field|Detail|
|----------|------|
|Degree programme|All Bachelor and Master programmes|
|Name of module|Open Source Hardware – Basics, Prototyping and Business Model Generation|
|Duration|1 Semester|
|Maximum participants|30|
|Language of instruction|English|
|ECTS-Points|3…5, depending on workload|
|Grading|Graded or pass/fail|
|Student workload|Overall workload 100/?/? hours (100/?/?%)<br />Attendance time 20/28/32 hours (20/?/?%)<br />Self study / lab / workshop 80/?/? hours (80/?/?%)<br />_Numbers depend on overall workload. Should be flexible.<br /> Attendance time depends on the form of lab or workshop: If supervised, this counts as attendance time._|
|SWS|5|
|Learning and teaching methods|- excursions<br />- groupwork<br />- simulation game<br />- problem-based learning<br />- project work<br />- project development<br />- role play<br />- lecture|
|Time and duration of exam|portfolio exam:<br /><br />documentation of fieldwork (development of a concept)<br />presentation (pitch)<br />weighting: 2:1
|Constructive Alignment|After completing the module, students will have a deeper understanding of processes and special conditions tied to the development of open source technologies. They will know to what extent open source companies differ from non-open source companies  and have gained knowledge of different forms of licenses and patents. They will have transformed a technical, artistic, social or economic idea into a project. The result of this project is either a product or service or a prototype or MVP (minimal valuable product) leading up to a hypothetical business model. The decisive steps, specifications and guidelines are documented in a way that is comprehensible to others.<br /><br />To verify the competencies, portfolios are created consisting of intermediate stages of the work on the project including documentation, reflective essays on the learning and cognitive processes and the final product. The results will be presented.<br /><br />The theoretical foundations are made available in the form of face-to-face and/or online lectures and as materials for self-study. Methodological foundations are laid in the form of individual or group work. The concrete work on the projects is done in supervised group work, mainly in laboratories and workshops. Cooperation with the Open Source community is explicitly desired.<br /><br />The module can be accompanied with internships in selected Open Source Hardware Projects, hence the module is optionally dual for the individual student.|
